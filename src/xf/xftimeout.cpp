#include "xftimeout.h"

XFTimeout::XFTimeout(int id, int interval, IXFReactive *pBehavior):XFEvent(XFEvent::Timeout, id, pBehavior)
{
    this->_interval = interval;
}

XFTimeout::~XFTimeout()
{

}

bool XFTimeout::operator ==(const XFTimeout &timeout) const
{
    return this->_id == timeout.getId() && this->_pBehavior == timeout.getBehavior();
}

bool XFTimeout::deleteAfterConsume()
{
    return true; //the timouts are always created by the timout manager and deleted by the event loop after being dispatched.
}
