<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="class">
    <name>EventStatus</name>
    <filename>class_event_status.html</filename>
    <member kind="enumeration">
      <type></type>
      <name>eEventStatus</name>
      <anchorfile>class_event_status.html</anchorfile>
      <anchor>a27a43970201d81a306b75cc8ceae3653</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>Unknown</name>
      <anchorfile>class_event_status.html</anchorfile>
      <anchor>a27a43970201d81a306b75cc8ceae3653aa2a25ce44055dac8250df1c77300bbd7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>Consumed</name>
      <anchorfile>class_event_status.html</anchorfile>
      <anchor>a27a43970201d81a306b75cc8ceae3653aabeec0355b7700d336a73ce13cab72ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>NotConsumed</name>
      <anchorfile>class_event_status.html</anchorfile>
      <anchor>a27a43970201d81a306b75cc8ceae3653ae4a94a4da28b6e50d20363965eb630f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>RegionFinished</name>
      <anchorfile>class_event_status.html</anchorfile>
      <anchor>a27a43970201d81a306b75cc8ceae3653ac3f26518396a5d14b09ff0221af3d918</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>Terminate</name>
      <anchorfile>class_event_status.html</anchorfile>
      <anchor>a27a43970201d81a306b75cc8ceae3653a0cb76744113cb786913a7279003ede2b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>EventStatus</name>
      <anchorfile>class_event_status.html</anchorfile>
      <anchor>a5a4e5707fd4bb7566e5dac67b4453151</anchor>
      <arglist>(eEventStatus eventStatus=Unknown)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>class_event_status.html</anchorfile>
      <anchor>aa0f3ca3fd15adf3c61b0938c02cf3ccd</anchor>
      <arglist>(const EventStatus::eEventStatus &amp;eventStatus) const </arglist>
    </member>
    <member kind="variable">
      <type>eEventStatus</type>
      <name>_status</name>
      <anchorfile>class_event_status.html</anchorfile>
      <anchor>aa32dcd60c7579771dcb6d2202ecd80e9</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>InitialEvent</name>
    <filename>class_initial_event.html</filename>
    <base>XFEvent</base>
    <member kind="enumeration">
      <type></type>
      <name>eEventType</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>a3a006b7f306bf9b90875e94ad1587479</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>Unknown</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>a3a006b7f306bf9b90875e94ad1587479adfc2ad39d30f7a5df70c664bb2e24542</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>Initial</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>a3a006b7f306bf9b90875e94ad1587479adc2df751813b38295784e246f9fe230e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>NullTransition</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>a3a006b7f306bf9b90875e94ad1587479af56340e726637274b5be2ba17d91a80a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>Event</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>a3a006b7f306bf9b90875e94ad1587479a4eefc024e7a72ce987c701013de3e100</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>Timeout</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>a3a006b7f306bf9b90875e94ad1587479a04f01a94d013ac0b3cd810b556427496</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>Terminate</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>a3a006b7f306bf9b90875e94ad1587479a05421c9d2b62f31c8aff06a0a9013dc7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>XFEvent</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>a9e1fde3a88653188dc413d31007a20df</anchor>
      <arglist>(eEventType eventType, int id, IXFReactive *pBehavior)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~XFEvent</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>a81e1850b46acd477ea5a9475e12e3e1e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>eEventType</type>
      <name>getEventType</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>aae43e275f7f2e356fb0809844a5c2c8d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>IXFReactive *</type>
      <name>getBehavior</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>a068eb0e0e4f791826b6ed6bc655ffda6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setBehavior</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>a2b4563d6217d766a1347dacda39dcb78</anchor>
      <arglist>(IXFReactive *pBehavior)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getId</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>a4dfe0519b2e1e8c140dcaec01303fcfa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>deleteAfterConsume</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>aeea5d56f34ae19b89d280c1b205026ee</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>const eEventType</type>
      <name>_eventType</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>ac669a129c41fbee348cb7fdbc09b9664</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>_id</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>a05d1045ac5f16d50d1d265e7b812b634</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>IXFReactive *</type>
      <name>_pBehavior</name>
      <anchorfile>class_x_f_event.html</anchorfile>
      <anchor>adc42a870b8b0069a71c8e6a4e1b09739</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>IXFReactive</name>
    <filename>class_i_x_f_reactive.html</filename>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>startBehavior</name>
      <anchorfile>class_i_x_f_reactive.html</anchorfile>
      <anchor>a7089e21bd2ba301cad8fe49a91eb3883</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>pushEvent</name>
      <anchorfile>class_i_x_f_reactive.html</anchorfile>
      <anchor>a860e839c5e8d31d1a88746c6f5aa5efd</anchor>
      <arglist>(XFEvent *pEvent)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual EventStatus</type>
      <name>process</name>
      <anchorfile>class_i_x_f_reactive.html</anchorfile>
      <anchor>ad0e7419f8cf188105e51cbd55c4a6298</anchor>
      <arglist>(XFEvent *pEvent)=0</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>XF</name>
    <filename>class_x_f.html</filename>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>init</name>
      <anchorfile>class_x_f.html</anchorfile>
      <anchor>a466a550b7d945c0d7241d677ace2cd7c</anchor>
      <arglist>(int timeInterval=10)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>start</name>
      <anchorfile>class_x_f.html</anchorfile>
      <anchor>a6b9d9deb30990a7fb2a2def472038223</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static XFThread *</type>
      <name>getMainThread</name>
      <anchorfile>class_x_f.html</anchorfile>
      <anchor>ab11bcb2991fe5096ddc45ed1a0e32b60</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected" static="yes">
      <type>static bool</type>
      <name>_bInitialized</name>
      <anchorfile>class_x_f.html</anchorfile>
      <anchor>a21257b2677b4a7b23604b6f80cdcde70</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected" static="yes">
      <type>static XFThread</type>
      <name>_mainThread</name>
      <anchorfile>class_x_f.html</anchorfile>
      <anchor>ac0289e8b2e48a44f2126001810a38b74</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>XFCustomEvent</name>
    <filename>class_x_f_custom_event.html</filename>
    <base>XFEvent</base>
    <member kind="function">
      <type></type>
      <name>XFCustomEvent</name>
      <anchorfile>class_x_f_custom_event.html</anchorfile>
      <anchor>ac5f709d2cec4e99f2df4f3c7ab34a0b2</anchor>
      <arglist>(int id, IXFReactive *pBehavior=NULL)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>deleteAfterConsume</name>
      <anchorfile>class_x_f_custom_event.html</anchorfile>
      <anchor>ae925d10a77f83713efe31c41e55ad10e</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>XFEvent</name>
    <filename>class_x_f_event.html</filename>
  </compound>
  <compound kind="class">
    <name>XFNullTransition</name>
    <filename>class_x_f_null_transition.html</filename>
    <base>XFEvent</base>
    <member kind="function">
      <type></type>
      <name>XFNullTransition</name>
      <anchorfile>class_x_f_null_transition.html</anchorfile>
      <anchor>a69f6f5584829433b265618d64be9cd88</anchor>
      <arglist>(IXFReactive *pBehavior=NULL)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>XFReactive</name>
    <filename>class_x_f_reactive.html</filename>
    <base>IXFReactive</base>
    <member kind="function">
      <type></type>
      <name>XFReactive</name>
      <anchorfile>class_x_f_reactive.html</anchorfile>
      <anchor>ab79741c25f17730dcb71ab168f4be620</anchor>
      <arglist>(XFThread *pThread=NULL)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>startBehavior</name>
      <anchorfile>class_x_f_reactive.html</anchorfile>
      <anchor>a8e0b223994673f0ed7e7c4658eebaeac</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>pushEvent</name>
      <anchorfile>class_x_f_reactive.html</anchorfile>
      <anchor>afd6a865891f5d985493212c21a40ca0e</anchor>
      <arglist>(XFEvent *pEvent)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual EventStatus</type>
      <name>processEvent</name>
      <anchorfile>class_x_f_reactive.html</anchorfile>
      <anchor>a299f10030e7ff8c3b04ae4fcf4fcbb46</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual XFThread *</type>
      <name>getThread</name>
      <anchorfile>class_x_f_reactive.html</anchorfile>
      <anchor>aa0771dbf3caf6c48fc9502b7941a00e1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>XFEvent *</type>
      <name>getCurrentEvent</name>
      <anchorfile>class_x_f_reactive.html</anchorfile>
      <anchor>a0b025a39630c1427df3203ebe103913d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>XFTimeout *</type>
      <name>getCurrentTimeout</name>
      <anchorfile>class_x_f_reactive.html</anchorfile>
      <anchor>a59e9ebd0bec66e17513c0b61eca56a4d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>scheduleTimeout</name>
      <anchorfile>class_x_f_reactive.html</anchorfile>
      <anchor>a84a999c727f5dad83078b76b21916e5b</anchor>
      <arglist>(int timeoutId, int interval)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>unscheduleTimeout</name>
      <anchorfile>class_x_f_reactive.html</anchorfile>
      <anchor>aced2b53888355e3a9228806be2b6947c</anchor>
      <arglist>(int timeoutId)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>startSubStateMachine</name>
      <anchorfile>class_x_f_reactive.html</anchorfile>
      <anchor>ad466fc28b7b84e8980150272f1497fba</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>XFThread *</type>
      <name>_pThread</name>
      <anchorfile>class_x_f_reactive.html</anchorfile>
      <anchor>a00363204d9739288102a2cc5a10d072e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>XFEvent *</type>
      <name>_pCurrentEvent</name>
      <anchorfile>class_x_f_reactive.html</anchorfile>
      <anchor>a9f5451411a9a8ae57ebdd0dfb004e014</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>XFThread</name>
    <filename>class_x_f_thread.html</filename>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>class_x_f_thread.html</anchorfile>
      <anchor>a3f05ac75e8fd004a800c10d864d61742</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>pushEvent</name>
      <anchorfile>class_x_f_thread.html</anchorfile>
      <anchor>a78221d14bd4d1095da9a3136df6e6505</anchor>
      <arglist>(XFEvent *pEvent)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>scheduleTimeout</name>
      <anchorfile>class_x_f_thread.html</anchorfile>
      <anchor>a805cb9667dd029b2f23a38e70a4f2647</anchor>
      <arglist>(int timeoutId, int interval, IXFReactive *pReactive)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unscheduleTimeout</name>
      <anchorfile>class_x_f_thread.html</anchorfile>
      <anchor>ada7e8628deadea00965e3a2376aba0ef</anchor>
      <arglist>(int timeoutId, IXFReactive *pReactive)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>execute</name>
      <anchorfile>class_x_f_thread.html</anchorfile>
      <anchor>a6499d600e0f31352786b614b22268d22</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>XFTimeoutManager *</type>
      <name>getTimeoutManager</name>
      <anchorfile>class_x_f_thread.html</anchorfile>
      <anchor>a64310e36725924c892846394b3faf2ca</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>dispatchEvent</name>
      <anchorfile>class_x_f_thread.html</anchorfile>
      <anchor>a7905792e267b37e7b0fc305409fb62bb</anchor>
      <arglist>(XFEvent *pEvent) const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>_bExecuting</name>
      <anchorfile>class_x_f_thread.html</anchorfile>
      <anchor>a4949d49682bce71c08864c65df80d4d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>queue&lt; XFEvent * &gt;</type>
      <name>_events</name>
      <anchorfile>class_x_f_thread.html</anchorfile>
      <anchor>a26b8b5eaaeb374df815091dffd9f0141</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>XFTimeout</name>
    <filename>class_x_f_timeout.html</filename>
    <base>XFEvent</base>
    <member kind="function">
      <type></type>
      <name>XFTimeout</name>
      <anchorfile>class_x_f_timeout.html</anchorfile>
      <anchor>a8c596eef9d1c6a2d603ff495e77016dc</anchor>
      <arglist>(int id, int interval, IXFReactive *pBehavior)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>class_x_f_timeout.html</anchorfile>
      <anchor>a54c1b4766cdc87a8d9b6a07d242fbc93</anchor>
      <arglist>(const XFTimeout &amp;timeout) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>deleteAfterConsume</name>
      <anchorfile>class_x_f_timeout.html</anchorfile>
      <anchor>a0aa94ed7c153713e5fb17bbe53a824bc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>_interval</name>
      <anchorfile>class_x_f_timeout.html</anchorfile>
      <anchor>a28b03dba8dd82e9d9aecfbee822a11b3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>_relTicks</name>
      <anchorfile>class_x_f_timeout.html</anchorfile>
      <anchor>abda72ce0831b9a8f73ae3d9e28b428cc</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>XFTimeoutManager</name>
    <filename>class_x_f_timeout_manager.html</filename>
    <member kind="function">
      <type>void</type>
      <name>setTickInterval</name>
      <anchorfile>class_x_f_timeout_manager.html</anchorfile>
      <anchor>ad24b69ca7a2c1a6ceef1e056d1e50c04</anchor>
      <arglist>(int tickInterval)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>class_x_f_timeout_manager.html</anchorfile>
      <anchor>af7058fea68818d3df49909642c2608c8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>scheduleTimeout</name>
      <anchorfile>class_x_f_timeout_manager.html</anchorfile>
      <anchor>a77370d98a9be04e5295f84f930e526bc</anchor>
      <arglist>(int timeoutId, int interval, IXFReactive *pReactive)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unscheduleTimeout</name>
      <anchorfile>class_x_f_timeout_manager.html</anchorfile>
      <anchor>a5e43de2dab7875206dd5c1e08f277a65</anchor>
      <arglist>(int timeoutId, IXFReactive *pReactive)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>tick</name>
      <anchorfile>class_x_f_timeout_manager.html</anchorfile>
      <anchor>a3734dd9d37f4d569fafea64ce8b1eece</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static XFTimeoutManager *</type>
      <name>getInstance</name>
      <anchorfile>class_x_f_timeout_manager.html</anchorfile>
      <anchor>a922cbea0143419cf1381727e6da5cfc5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>addTimeout</name>
      <anchorfile>class_x_f_timeout_manager.html</anchorfile>
      <anchor>a23915d0672e3db02b5e1b3e847248828</anchor>
      <arglist>(XFTimeout *pNewTimeout)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>removeTimeouts</name>
      <anchorfile>class_x_f_timeout_manager.html</anchorfile>
      <anchor>a8afcdc14de56e35354a7d287bbcd6c27</anchor>
      <arglist>(int timeoutId, IXFReactive *pReactive)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>returnTimeout</name>
      <anchorfile>class_x_f_timeout_manager.html</anchorfile>
      <anchor>ab2cde664b85877cba3f08f13341598ff</anchor>
      <arglist>(XFTimeout *pTimeout)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>startHardwareTimer</name>
      <anchorfile>class_x_f_timeout_manager.html</anchorfile>
      <anchor>a867e1e2959aef429886e7029ab6d4c68</anchor>
      <arglist>(int tickTime)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>_tickInterval</name>
      <anchorfile>class_x_f_timeout_manager.html</anchorfile>
      <anchor>aff02faba02f1f613c0fd472c5d6561bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>TimeoutList</type>
      <name>_timeouts</name>
      <anchorfile>class_x_f_timeout_manager.html</anchorfile>
      <anchor>a59bf23aa8c89212ab00f53f62e7405c0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected" static="yes">
      <type>static XFTimeoutManager *</type>
      <name>_pInstance</name>
      <anchorfile>class_x_f_timeout_manager.html</anchorfile>
      <anchor>a07e2fe238b5f4870a752234f37788270</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="page">
    <name>index</name>
    <title>PTR Execution Framework Documentation</title>
    <filename>index</filename>
    <docanchor file="index" title="Introduction">sec_xf_intro</docanchor>
    <docanchor file="index" title="XF Class Diagram">sec_xf_cmd</docanchor>
    <docanchor file="index" title="Starting Point">sec_xf_start</docanchor>
    <docanchor file="index" title="Links to related Documentation">sec_xf_external_links</docanchor>
  </compound>
</tagfile>
