#ifndef XFTHREAD_H
#define XFTHREAD_H

#include <queue>
#include "xfevent.h"

#ifdef TC_QTCREATOR
  #include <QThread>
#endif // TC_QTCREATOR

#ifdef TC_QTCREATOR
    #include <QMutex>
#endif

using namespace std;

class XFTimeoutManager;

/**
 * XFThread representing the instance executing the behavior. In a OS less XF,
 * only one instance of XFThread gets created (see XF::_mainThread). In a XF
 * interfacing an OS, multiple instances can be made, each interfacing the real
 * OS thread.
 */
class XFThread
{
public:
	XFThread();
	virtual ~XFThread();

    void start();										///< Starts the thread.
	void stop();
	void pushEvent(XFEvent * pEvent);					///< Adds event to the #_events queue.

	/**
	 * Adds a new timeout to be handled. The XFThread will forward the timeout
	 * information to the timeout manager which is responsible to handle all timeouts.
	 */
	void scheduleTimeout(int timeoutId, int interval, IXFReactive * pReactive);

	/**
	 * Removes all timeouts corresponding the given parameters.
	 */
	void unscheduleTimeout(int timeoutId, IXFReactive * pReactive);

protected:

	/**
	 * Main loop of the thread. Implements event loop processing.
	 */
	void execute();
	XFTimeoutManager * getTimeoutManager() const;		///< Returns pointer to timeout manager.

	/**
	 * Dispatchs the event to the corresponding behavioral part. For example
	 * the state machine which sould process the event.
	 * \param pEvent The event to dispatch
	 */
	void dispatchEvent(XFEvent * pEvent) const;


#ifdef TC_QTCREATOR
    QMutex mutex;
    friend class ThreadExec;
#endif

protected:
	bool _bExecuting;				///< True as long as the thread is executing the main loop.

	queue<XFEvent *> _events;		///< Queue holding events waiting to get dispatched.
};

#ifdef TC_QTCREATOR
    class ThreadExec : public QThread
    {
    public:
        ThreadExec(XFThread* thread) {this->thread = thread;}
        virtual ~ThreadExec() { thread->stop(); wait(1000); }
    protected:
        XFThread* thread;
    virtual void run () { thread->execute(); }
    };
#endif // TC_QTCREATOR


#endif // XFTHREAD_H
