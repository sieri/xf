#-------------------------------------------------
#
# Project created by QtCreator 2010-11-19T08:02:03
#
#-------------------------------------------------

QT       += core
QT       -= gui

TARGET = xf
TEMPLATE = lib
CONFIG += staticlib
CONFIG -= app_bundle
CONFIG += c++11

DEFINES += TC_QTCREATOR

INCLUDEPATH += . \
	../trace

!win32{
CONFIG(debug, debug|release) {
	DESTDIR = debug
	OBJECTS_DIR = debug/obj
	MOC_DIR = debug/moc
} else {
	DESTDIR = release
	OBJECTS_DIR = release/obj
	MOC_DIR = release/moc
}
}

SOURCES += \
	xf.cpp \
	xftimeoutmanager.cpp \
	xfthread.cpp \
	xfreactive.cpp \
	xftimeout.cpp \
	initialevent.cpp \
	xfcustomevent.cpp \
	../trace/trace.cpp \
	xfnulltransition.cpp

HEADERS += \
	xf.h \
	xftimeoutmanager.h \
	ixfevent.h \
	xfthread.h \
	ixfreactive.h \
	xfreactive.h \
	xfeventstatus.h \
	xftimeout.h \
	initialevent.h \
	xfcustomevent.h \
	../trace/trace.h \
	xfnulltransition.h
