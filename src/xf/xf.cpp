#include "xf.h"
#include "xftimeoutmanager.h"
#include <../critical/critical.h>

bool XF::_bInitialized = false;			///< Changes from false to true after calling method init(int). Used to handle multiple calls to init(int).
XFThread XF::_mainThread;

void XF::init(int timeInterval)
{
    if(_bInitialized)
    {
       // throw "Already Initialized"; //
       ///\todo Do some proper exception handling and throwing.
    }
    else
    {
        _bInitialized = true;
        XFTimeoutManager::getInstance()->setTickInterval(timeInterval); //configure the timoutManager
    }
}

void XF::start()
{
    if(!_bInitialized)
    {
        //throw "Initialize the XF before starting";
        ///\todo do some proper error handling
    }
    else
    {
        //start the timout manager
        XFTimeoutManager::getInstance()->start();


        //on ARMEBS4 as there is no OS this is a blocking call as long as the event loop is runing.
        getMainThread()->start();

    }
}

XFThread *XF::getMainThread()
{
    return &_mainThread;
}

XFThread *XF::createThread()
{
#ifdef BOARD_ARMEBS4
    return getMainThread();
#else
    return new XFThread();
#endif
}

