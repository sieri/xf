#ifdef TC_QTCREATOR
	#include <QTimerEvent>
#endif // TC_QTCREATOR
#ifdef BOARD_ARMEBS4
	#include "config/config.h"
	#include <critical.h>
#endif // BOARD_ARMEBS4
#include "xftimeoutmanager.h"
#include "ixfreactive.h"
#include "xftimeout.h"

XFTimeoutManager * XFTimeoutManager::_pInstance = NULL;

XFTimeoutManager::~XFTimeoutManager()
{
    //delete all the created timeouts
    for(auto i : _timeouts)
    {
        delete i;
    }
}

XFTimeoutManager *XFTimeoutManager::getInstance()
{
    if(_pInstance == NULL)
    {
        _pInstance = new XFTimeoutManager();
    }
    return _pInstance;
}

void XFTimeoutManager::setTickInterval(int tickInterval)
{
    _tickInterval = tickInterval;

}

void XFTimeoutManager::start()
{
    //QT implementation of the timer
    #ifdef TC_QTCREATOR
        _timerId = startTimer(_tickInterval);
    #endif

   //start the timer on ARMEBS4
	#ifdef BOARD_ARMEBS4
        startHardwareTimer(_tickInterval);
	#endif
}

void XFTimeoutManager::scheduleTimeout(int timeoutId, int interval, IXFReactive *pReactive)
{
    addTimeout(new XFTimeout(timeoutId, interval, pReactive));
}

void XFTimeoutManager::unscheduleTimeout(int timeoutId, IXFReactive *pReactive)
{
    removeTimeouts(timeoutId, pReactive);
}

void XFTimeoutManager::tick()
{
    //if there is no timers shedulded then there is nothing to do
    if(_timeouts.empty())
    {
        return;
    }


    XFTimeout* t = _timeouts.front();
    t->_relTicks -= _tickInterval;
    if(t->_relTicks <= 0) //decrement and test if timout is to be returned.
    {
        int overshoot = _timeouts.front()->_relTicks; //the number of ticks the timer went bellow 0
        returnTimeout(_timeouts.front());
        //check the next timeout if to see if the next timouts are with the same interval
        while (!_timeouts.empty() && _timeouts.front()->_relTicks + overshoot <= 0) {
            overshoot = _timeouts.front()->_relTicks + overshoot;
            returnTimeout(_timeouts.front());
        }
    }

}

XFTimeoutManager::XFTimeoutManager()
{

}

void XFTimeoutManager::addTimeout(XFTimeout *pNewTimeout)
{
#ifdef BOARD_ARMEBS4
    enterCritical();
#endif

#ifdef TC_QTCREATOR
    mutex.lock();
#endif

	TimeoutList::iterator it = _timeouts.begin();

    int aggregateTime = 0;
    bool inserted = false;

    while (it != _timeouts.end() && !inserted) {
        if(aggregateTime + (*it)->_relTicks <= pNewTimeout->_interval)
        {
            //increment to next
            aggregateTime += (*it)->_relTicks;
        }
        else
        {

            //the new timeout need to be inserted here.
            it = _timeouts.insert(it, pNewTimeout);

            inserted = true; //exit the loop
        }
        it++;
    }

    int delta = pNewTimeout->_interval - aggregateTime;
    pNewTimeout->_relTicks = delta;

    if(inserted)
    {
        //update all the following timeouts
        (*it)->_relTicks -= delta;

    }
    else
    {
        //append the new timeout at the end
        _timeouts.push_back(pNewTimeout);
    }
#ifdef BOARD_ARMEBS4
    exitCritical();
#endif

#ifdef TC_QTCREATOR
    mutex.unlock();
#endif
}

void XFTimeoutManager::removeTimeouts(int timeoutId, IXFReactive *pReactive)
{
#ifdef BOARD_ARMEBS4
    enterCritical();
#endif

#ifdef TC_QTCREATOR
    mutex.lock();
#endif
    XFTimeout timemout(timeoutId,0,pReactive); //create a temp dummy timeout to render the comparison more straight forward
    TimeoutList::iterator it = _timeouts.begin();

    int delta = 0;

    while (it != _timeouts.end()) {
        if(timemout == **it)
        {
            //remove the timeout
            delta += (*it)->_relTicks;
            delete *it; //delete the timeout referenced
            it = _timeouts.erase(it); //and remove the element from the list
        }
        else
        {
            //remove the time from deleted elements.
            (*it)->_relTicks -= delta;
            it++; //check the next timeout
        }
    }

#ifdef BOARD_ARMEBS4
    exitCritical();
#endif

#ifdef TC_QTCREATOR
    mutex.unlock();
#endif

}

void XFTimeoutManager::returnTimeout(XFTimeout *pTimeout)
{
    pTimeout->getBehavior()->pushEvent(pTimeout); //push the even to the queue.

    _timeouts.pop_front();
}

//handle timout manager for Armebs 4
#ifdef BOARD_ARMEBS4
	void XFTimeoutManager::startHardwareTimer(int tickTime)
	{
				   const uint32_t tickRateInHz = 1000/tickTime;

				   bool error = SysTick_Config(SystemClock.hclk / tickRateInHz);
				   UNUSED(error);

				   ASSERT(!error);
	}

	extern "C" void SysTick_Handler()
	{
				   XFTimeoutManager::getInstance()->tick();          // Call Framework hook tick function
	}
#endif // BOARD_ARMEBS4

//handle timer for qt
#ifdef TC_QTCREATOR
void XFTimeoutManager::timerEvent(QTimerEvent *event)
{
    tick();
}
#endif
