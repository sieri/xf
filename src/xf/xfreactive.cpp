#include "xf.h"
#include "xfreactive.h"
#include "xftimeout.h"
#include "initialevent.h"

XFReactive::XFReactive(XFThread *pThread)
{
    //if no thread where set we use the main one by default.
    if(pThread == NULL)
    {
        this->_pThread = XF::getMainThread();
    }
    else
    {
        this->_pThread = pThread;
    }
}

XFReactive::~XFReactive()
{

}

void XFReactive::startBehavior()
{
    GEN(InitialEvent()); //Start the behaviour by sending an initial event
}

void XFReactive::pushEvent(XFEvent *pEvent)
{
   pEvent->setBehavior(this); //set the behaviour correctly
   //and transmit it to the
   _pThread->pushEvent(pEvent);
}

EventStatus XFReactive::processEvent()
{
    //no behavours are implemented if the hardware isn't used
    return EventStatus::NotConsumed;
}

XFThread *XFReactive::getThread()
{
    return _pThread;
}

XFEvent *XFReactive::getCurrentEvent() const
{
    return _pCurrentEvent;
}

XFTimeout *XFReactive::getCurrentTimeout()
{
    //if the current event is a timeout we can return it casted
    if(_pCurrentEvent->getEventType() == XFEvent::Timeout)
    {
        return (XFTimeout*) _pCurrentEvent;
    }
    else
    {
        //return null if it wasn't a timeout
        return NULL;
    }
}

void XFReactive::setCurrentEvent(XFEvent *pEvent)
{
    _pCurrentEvent = pEvent;
}

EventStatus XFReactive::process(XFEvent *pEvent)
{
    //set the current event and then process it
    setCurrentEvent(pEvent);
    return processEvent();
}
