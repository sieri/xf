#ifdef TC_QTCREATOR //if on QT
    #include <QThread>
#endif

#ifdef BOARD_ARMEBS4
	#include <critical.h>
#endif

#include "xfthread.h"
#include "ixfreactive.h"
#include "xftimeoutmanager.h"

XFThread::XFThread()
{
    _bExecuting = false;
}

XFThread::~XFThread()
{
    //empty the list delelting all queued events where delete after consume is true
    while (!_events.empty()) {
        XFEvent *e = _events.front();
        if(e->deleteAfterConsume())
        {
            delete e;
        }
        _events.pop();
    }
}

void XFThread::start()
{
    if(_bExecuting == false)
    {
        _bExecuting = true;

            //simply run the execute function until the event loop is stopped on ARMEBS
        #ifdef BOARD_ARMEBS4
            execute();
        #endif

            //start the execution thread on QT on an other new thread
        #ifdef TC_QTCREATOR
            ThreadExec* thread = new ThreadExec(this);
            thread->start();
        #endif
    }
}

void XFThread::stop()
{
    _bExecuting = false;
}

void XFThread::pushEvent(XFEvent *pEvent)
{
//do the correct concurent modification protection for the plateform
#ifdef BOARD_ARMEBS4
    enterCritical();
#endif

#ifdef TC_QTCREATOR
    mutex.lock();
#endif

    //and push the event
    _events.push(pEvent);

#ifdef BOARD_ARMEBS4
    exitCritical();
#endif

#ifdef TC_QTCREATOR
    mutex.unlock();
#endif
}

void XFThread::scheduleTimeout(int timeoutId, int interval, IXFReactive *pReactive)
{
    getTimeoutManager()->scheduleTimeout(timeoutId, interval, pReactive);
}

void XFThread::unscheduleTimeout(int timeoutId, IXFReactive *pReactive)
{
    getTimeoutManager()->unscheduleTimeout(timeoutId, pReactive);
}

void XFThread::execute()
{
    while (_bExecuting) { //as long as we need to execute the thread
        //pop the top event in the list
       if(!_events.empty())
       {
           //protect the list from concurent accesses
           #ifdef BOARD_ARMEBS4
                enterCritical();
           #endif

           #ifdef TC_QTCREATOR
               mutex.lock();
           #endif

    	   XFEvent* p = _events.front();
		   _events.pop();

            #ifdef BOARD_ARMEBS4
                exitCritical();
            #endif

            #ifdef TC_QTCREATOR
                mutex.unlock();
            #endif
            // dispatch need to be outside of the protection as
            //the event is already outside the list and we have no control over the execution time
    	   dispatchEvent(p);
       }
    }

}

XFTimeoutManager *XFThread::getTimeoutManager() const
{
    return XFTimeoutManager::getInstance();
}

void XFThread::dispatchEvent(XFEvent *pEvent) const
{
    //process the event
   EventStatus status = pEvent->getBehavior()->process(pEvent);

   //terminate the behavior if needed
   if(status.is(EventStatus::Terminate))
   {
       delete pEvent->getBehavior();
   }

    //delete the event if needed
   if(pEvent->deleteAfterConsume())
   {
       delete pEvent;
   }
}


