
QT       += core
QT       -= gui

TARGET = test4
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++11

DEFINES += TC_QTCREATOR

DEPENDPATH += . \

INCLUDEPATH += . \
	../../src/xf \
	../../src/trace

# Add configuration parameters to add real dependency to library(-ies)
CONFIG(debug) {
	LIBS += -L../../src/xf/debug
	PRE_TARGETDEPS += ../../src/xf/debug/libxf.a
}
else {
	LIBS += -L../../src/xf/release
	PRE_TARGETDEPS += ../../src/xf/release/libxf.a
}

CONFIG(debug, debug|release):LIBS += ../../src/xf/debug/libxf.a
CONFIG(release, debug|release):LIBS += .../../src/xf/release/libxf.a


SOURCES += main.cpp \
    testfactory04.cpp \
    statemachine04a.cpp \
    evrestart.cpp \
    statemachine04b.cpp

HEADERS += \
    testfactory04.h \
    statemachine04a.h \
    eventids.h \
    evrestart.h \
    statemachine04b.h
