#include <trace.h>
#include "testfactory04.h"

TestFactory04::TestFactory04()
{
	Trace::out("Starting test4...\n---------------------");

	// Set up association(s)
	_task01.setNeighbour(&_task02);

	// Start state machine(s)
	_task01.startBehavior();
	_task02.startBehavior();
}
