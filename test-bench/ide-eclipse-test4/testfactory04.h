#ifndef TESTFACTORY04_H
#define TESTFACTORY04_H


#include "statemachine04a.h"
#include "statemachine04b.h"


/**
 * \ingroup test04
 *
 * Factory creating all objects used in test4.
 *
 */
class TestFactory04
{
public:
	TestFactory04();			///< Constructor

protected:
	StateMachine04a _task01;		///< Instance of StateMachine04a
	StateMachine04b _task02;		///< Instance of StateMachine04b
};

#endif // TESTFACTORY03_H
