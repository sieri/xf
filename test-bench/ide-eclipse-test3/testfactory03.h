#ifndef TESTFACTORY03_H
#define TESTFACTORY03_H


#include "statemachine03.h"


/**
 * \ingroup test03
 *
 * Factory creating all objects used in test3.
 *
 */
class TestFactory03
{
public:
	TestFactory03();			///< Constructor

protected:
	StateMachine03 _task01;		///< Instance of StateMachine03

};

#endif // TESTFACTORY03_H
