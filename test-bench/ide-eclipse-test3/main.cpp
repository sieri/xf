#include <xf.h>
#ifdef TC_QTCREATOR
	#include <QCoreApplication>
#endif // TC_QTCREATOR
#include "testfactory03.h"


int main(int argc, char *argv[])
{
#ifdef TC_QTCREATOR
	QCoreApplication app(argc, argv);
#endif // TC_QTCREATOR

	XF::init(20);

	TestFactory03 factory;

	XF::start();

#ifdef TC_QTCREATOR
	return app.exec();
#else
	return 0;
#endif // TC_QTCREATOR
}
