#include <stdio.h>
#include <trace.h>
#include "statemachine03.h"
#include "evrestart.h"


StateMachine03::StateMachine03()
{
	_currentState = STATE_INITIAL;
}

StateMachine03::~StateMachine03()
{

}

/**
 * Implements state machine behavior.
 */
EventStatus StateMachine03::processEvent()
{
	eEventStatus eventStatus = EventStatus::Unknown;

	switch (_currentState)
	{
	case STATE_INITIAL:
		{
			if (getCurrentEvent()->getEventType() == XFEvent::Initial)
			{
				GEN(XFNullTransition());

				_currentState = STATE_WAIT;
				eventStatus = EventStatus::Consumed;
			}
		}
		break;
	case STATE_WAIT:
		{
			if (getCurrentEvent()->getEventType() == XFEvent::NullTransition ||
				(getCurrentEvent()->getEventType() == XFEvent::Event &&
				 getCurrentEvent()->getId() == EventId::evRestart))
			{
				{
					Trace::out("Wait");
				}

				getThread()->scheduleTimeout(Timeout_WAIT_id, 2000, this);

				_currentState = STATE_SEND_RESTART;
				eventStatus = EventStatus::Consumed;
			}
		}
		break;
	case STATE_SEND_RESTART:
		{
			if (getCurrentEvent()->getEventType() == XFEvent::Timeout &&
				getCurrentTimeout()->getId() == Timeout_WAIT_id)
			{
				{
					Trace::out("Wait restart");
					GEN(evRestart());
				}

				_currentState = STATE_WAIT;
				eventStatus = EventStatus::Consumed;
			}
		}
		break;
	default:
		break;
	}

	return eventStatus;
}
