#ifndef TESTFACTORY04_H
#define TESTFACTORY04_H


#include "statemachine06a.h"
#include "statemachine06b.h"


/**
 * \ingroup test06
 *
 * Factory creating all objects used in test6.
 *
 */
class TestFactory06
{
public:
    TestFactory06();			///< Constructor

protected:
    StateMachine06a* _task01;		///< Instance of StateMachine05a saying 'Tick 500ms'
    StateMachine06b* _task02;		///< Instance of StateMachine05b saying 'One' every second.

};

#endif // TESTFACTORY03_H
