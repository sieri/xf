#include <trace.h>
#include "testfactory06.h"

TestFactory06::TestFactory06()
{
    Trace::out("Starting test6...\n---------------------");

    XFThread* thread = XF::createThread();

    _task01 = new StateMachine06a(thread);
    _task02 = new StateMachine06b("ping");
    // Start state machines
    _task02->startBehavior();
    _task01->startBehavior();

    //we need to start the xf before starting secondary threads
    XF::start();
    thread->start();
}
