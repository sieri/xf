
#include <trace.h>
#include "StateMachine06a.h"
#include <stdint.h>

/**
 * Constructor
 *
 * \param text Text to display by the state machine.
 */
StateMachine06a::StateMachine06a(XFThread *thread):XFReactive(thread)
{
	_currentState = STATE_INITIAL;
}

StateMachine06a::~StateMachine06a()
{

}

EventStatus StateMachine06a::processEvent()
{
	eEventStatus eventStatus = EventStatus::Unknown;

	switch (_currentState)
	{
	case STATE_INITIAL:
		{
			if (getCurrentEvent()->getEventType() == XFEvent::Initial)
			{
				GEN(XFNullTransition());

                _currentState = STATE_BLOCK;

				eventStatus = EventStatus::Consumed;
			}
		}
		break;
    case STATE_BLOCK:
		{
			if (getCurrentEvent()->getEventType() == XFEvent::NullTransition ||
				(getCurrentEvent()->getEventType() == XFEvent::Timeout &&
                 getCurrentTimeout()->getId() == Timeout_Block_id))
			{
				{
                    Trace::out("Starting wait");
				}

				#ifdef TC_QTCREATOR
                	QThread::msleep(2000);


				#else

                	for(uint32_t i = 0; i < 30000000; i++)//timing absolutly not precise, I disgust myself.
                	{
                		continue;
                	}

                #endif
                {
					Trace::out("Ended wait");
				}

                getThread()->scheduleTimeout(Timeout_Block_id, 500, this);

                _currentState = STATE_BLOCK;

				eventStatus = EventStatus::Consumed;
			}
		}
		break;
	default:
		break;
	}

	return eventStatus;
}
