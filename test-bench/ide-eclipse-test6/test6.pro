
QT       += core
QT       -= gui

TARGET = test6
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++11

DEFINES += TC_QTCREATOR

DEPENDPATH += . \

INCLUDEPATH += . \
	../../src/xf \
	../../src/trace

# Add configuration parameters to add real dependency to library(-ies)
CONFIG(debug) {
	LIBS += -L../../src/xf/debug
	PRE_TARGETDEPS += ../../src/xf/debug/libxf.a
}
else {
	LIBS += -L../../src/xf/release
	PRE_TARGETDEPS += ../../src/xf/release/libxf.a
}

CONFIG(debug, debug|release):LIBS += ../../src/xf/debug/libxf.a
CONFIG(release, debug|release):LIBS += .../../src/xf/release/libxf.a


SOURCES += main.cpp \
    testfactory06.cpp \
    statemachine06a.cpp \
    statemachine06b.cpp \
	evrestart.cpp

HEADERS += \
    testfactory06.h \
    statemachine06a.h \
    statemachine06b.h \
    eventids.h \
	evrestart.h
