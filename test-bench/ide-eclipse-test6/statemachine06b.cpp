#include <stdio.h>
#include <trace.h>
#include "statemachine06b.h"

/**
 * Constructor
 *
 * \param text Text to display by the state machine.
 */
StateMachine06b::StateMachine06b(string text)
 : _text(text)
{
	_currentState = STATE_INITIAL;
}

StateMachine06b::~StateMachine06b()
{

}

EventStatus StateMachine06b::processEvent()
{
	eEventStatus eventStatus = EventStatus::Unknown;

	switch (_currentState)
	{
	case STATE_INITIAL:
		{
			if (getCurrentEvent()->getEventType() == XFEvent::Initial)
			{
				GEN(XFNullTransition());

				_currentState = STATE_SAY_TEXT;

				eventStatus = EventStatus::Consumed;
			}
		}
		break;
	case STATE_SAY_TEXT:
		{
			if (getCurrentEvent()->getEventType() == XFEvent::NullTransition ||
				(getCurrentEvent()->getEventType() == XFEvent::Timeout &&
				 getCurrentTimeout()->getId() == Timeout_SAY_HELLO_id))
			{
				{
					Trace::out(getText());
				}

                getThread()->scheduleTimeout(Timeout_SAY_HELLO_id, 500, this);

				_currentState = STATE_SAY_TEXT;

				eventStatus = EventStatus::Consumed;
			}
		}
		break;
	default:
		break;
	}

	return eventStatus;
}
