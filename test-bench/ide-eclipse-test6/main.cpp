#include <xf.h>
#ifdef TC_QTCREATOR
	#include <QCoreApplication>
#endif // TC_QTCREATOR
#include "testfactory06.h"


int main(int argc, char *argv[])
{
#ifdef TC_QTCREATOR
	QCoreApplication app(argc, argv);
#endif // TC_QTCREATOR

	XF::init(20);

    TestFactory06 factory;

    //start need to be done before starting the threads so the factory will call the XF thread
	//XF::start();

#ifdef TC_QTCREATOR
	return app.exec();
#else
	return 0;
#endif // TC_QTCREATOR
}
