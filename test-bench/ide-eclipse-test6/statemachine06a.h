#ifndef STATEMACHINE05A_H
#define STATEMACHINE05A_H

#include <xfreactive.h>
#include <string>


/**
 * \ingroup test06
 *
 * Task implementing a little state machine which simulate a long blocking task
 * by waiting 1000ms in the process event. Repeat when done after 500ms
 * Print the at the start and end of the wait.
 *
 */
class StateMachine06a : public XFReactive
{
public:
    StateMachine06a(XFThread* thread = NULL);
    virtual ~StateMachine06a();

protected:
	virtual EventStatus processEvent();									///< Remplementation from XFReactive

protected:
	/**
	 * Timeout identifier(s) for this state machine
	 */
	typedef enum
	{
        Timeout_Block_id = 1	///< Timeout id for WAIT
	} eTimeoutId;

	/**
	 * Enumeration used to have a unique identifier for every
	 * state in the state machine.
	 */
	typedef enum
	{
		STATE_UNKNOWN = 0,			///< Unknown state
		STATE_INITIAL = 1,			///< Initial state
        STATE_BLOCK = 2			///< Blocking task state
	} eMainState;

	eMainState _currentState;		///< Attribute indicating currently active state

};

#endif // STATEMACHINE05A_H
