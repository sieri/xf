#ifndef TESTFACTORY04_H
#define TESTFACTORY04_H


#include "statemachine05a.h"
#include "statemachine05b.h"


/**
 * \ingroup test05
 *
 * Factory creating all objects used in test5.
 *
 */
class TestFactory06
{
public:
	TestFactory06();			///< Constructor

protected:
	StateMachine06a _task01;		///< Instance of StateMachine05a saying 'Tick 500ms'
	StateMachine06b _task02;		///< Instance of StateMachine05b saying 'One' every second.
	StateMachine06b _task03;		///< Instance of StateMachine05b saying 'Two' every second.
	StateMachine06b _task04;		///< Instance of StateMachine05b saying 'Three' every second.
};

#endif // TESTFACTORY03_H
