#include <trace.h>
#include "testfactory05.h"

TestFactory06::TestFactory06()
 : _task01("Tick 500ms"),
   _task02(" One"),
   _task03("  Two"),
   _task04("   Three")
{
	Trace::out("Starting test5...\n---------------------");

	// Start state machine(s)
	_task01.startBehavior();
	_task02.startBehavior();
	_task03.startBehavior();
	_task04.startBehavior();
}
