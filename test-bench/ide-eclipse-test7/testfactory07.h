#ifndef TESTFACTORY04_H
#define TESTFACTORY04_H


#include "statemachine07a.h"


/**
 * \ingroup test07
 *
 * Factory creating all objects used in test7.
 *
 */
class TestFactory07
{
public:
    TestFactory07();			///< Constructor

protected:
    StateMachine07a* _task01;		///< Instance of StateMachine07a
    StateMachine07a* _task02;		///< Instance of StateMachine07a
    StateMachine07a* _task03;		///< Instance of StateMachine07a


};

#endif // TESTFACTORY03_H
