#include <trace.h>
#include "testfactory07.h"

TestFactory07::TestFactory07()
{
    Trace::out("Starting test7...\n---------------------");


    _task01 = new StateMachine07a(5, "1");
    _task02 = new StateMachine07a(10, "2");
    _task03 = new StateMachine07a(20, "3");
    // Start state machine
    _task01->startBehavior();
    _task02->startBehavior();
    _task03->startBehavior();


}
