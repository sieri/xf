#ifndef EVRESTART_H
#define EVRESTART_H


#include <xfcustomevent.h>
#include "eventids.h"


/**
 * Restart event used to reset a state machine
 */
class evStatic : public XFCustomEvent
{
public:
    evStatic();
    virtual ~evStatic();
};

#endif // EVRESTART_H
