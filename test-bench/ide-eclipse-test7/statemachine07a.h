#ifndef STATEMACHINE05A_H
#define STATEMACHINE05A_H

#include <xfreactive.h>
#include <string>
#include "evstatic.h"


/**
 * \ingroup test07
 *
 * Task implementing a state machine which switch state frequently
 * allowing to see more quickly if there is leaks in the memory due to non deleted
 * events.
 * treating the events returns various status to confirm that the xf doesn't have
 * a different behavour in the deletion with it.
 * one of the event is reused multiple times and shouldn't be deleted by the XF
 * if everything is ok
 *
 * the timer is also faster than what is given to the timer id to check if there
 * is a correct responce to timer overshoot.
 */
class StateMachine07a : public XFReactive
{
public:
    StateMachine07a(int repeatInterval, string text);
    virtual ~StateMachine07a();

protected:
	virtual EventStatus processEvent();									///< Remplementation from XFReactive

protected:
    inline int getRepeatInterval() const { return _repeatInterval; }	///< Returns repeat interval. Accessor for #_repeatInterval.
    inline string getText() const { return _text; }						///< Returns text. Accessor for #_text.



	/**
	 * Timeout identifier(s) for this state machine
	 */
	typedef enum
	{
        Timeout_next_id = 1         ///< Timeout id for WAIT
	} eTimeoutId;

	/**
	 * Enumeration used to have a unique identifier for every
	 * state in the state machine.
	 */
	typedef enum
	{
		STATE_UNKNOWN = 0,			///< Unknown state
		STATE_INITIAL = 1,			///< Initial state
        STATE_CONSUMING = 2,        ///< Blocking task state
        STATE_NOTCONSUMING,         ///< Not Consuming state
        STATE_RETURNUNKNOWN,        ///< Unknown state
        STATE_STATIC_EVENT          ///< static event
	} eMainState;

	eMainState _currentState;		///< Attribute indicating currently active state

    evStatic reusedEvent;           ///< The event reused multiple time by the state machine

    int _repeatInterval;			///< Interval in milliseconds to repeat text in state machine
    string _text;					///< Text to display in state machine state

};

#endif // STATEMACHINE05A_H
