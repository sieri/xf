
#include <trace.h>
#include "StateMachine07a.h"
#include <stdint.h>

/**
 * Constructor
 *
 * \param text Text to display by the state machine.
 */
StateMachine07a::StateMachine07a(int repeatInterval, string text):XFReactive()
{
	_currentState = STATE_INITIAL;
    this->_repeatInterval = repeatInterval;
    this->_text = text;
}

StateMachine07a::~StateMachine07a()
{

}

EventStatus StateMachine07a::processEvent()
{
	eEventStatus eventStatus = EventStatus::Unknown;

	switch (_currentState)
	{
	case STATE_INITIAL:
		{
			if (getCurrentEvent()->getEventType() == XFEvent::Initial)
			{
				GEN(XFNullTransition());

                _currentState = STATE_CONSUMING;

				eventStatus = EventStatus::Consumed;
			}
		}
		break;
    case STATE_CONSUMING:
		{
			if (getCurrentEvent()->getEventType() == XFEvent::NullTransition ||
				(getCurrentEvent()->getEventType() == XFEvent::Timeout &&
                 getCurrentTimeout()->getId() == Timeout_next_id))
			{
				{
                    Trace::out(getText());
				}



                getThread()->scheduleTimeout(Timeout_next_id, getRepeatInterval(), this);

                _currentState = STATE_NOTCONSUMING;

				eventStatus = EventStatus::Consumed;
			}
		}
        break;
    case STATE_NOTCONSUMING:
        {
            if (getCurrentEvent()->getEventType() == XFEvent::NullTransition ||
                (getCurrentEvent()->getEventType() == XFEvent::Timeout &&
                 getCurrentTimeout()->getId() == Timeout_next_id))
            {
                {
                    Trace::out(getText());
                }



                getThread()->scheduleTimeout(Timeout_next_id, getRepeatInterval(), this);

                _currentState = STATE_RETURNUNKNOWN;

                eventStatus = EventStatus::NotConsumed;
            }
        }
        break;
    case STATE_RETURNUNKNOWN:
        {
            if (getCurrentEvent()->getEventType() == XFEvent::NullTransition ||
                (getCurrentEvent()->getEventType() == XFEvent::Timeout &&
                 getCurrentTimeout()->getId() == Timeout_next_id))
            {
                {
                    Trace::out(getText());
                }



                pushEvent(&reusedEvent);

                _currentState = STATE_STATIC_EVENT;

                eventStatus = EventStatus::Unknown;
            }
        }
        break;
    case STATE_STATIC_EVENT:
        {
            if(getCurrentEvent()->getId() == EventId::evStatic)
            {
                Trace::out(getText());

                {
                    Trace::out(getText());
                }



                getThread()->scheduleTimeout(Timeout_next_id, getRepeatInterval(), this);

                _currentState = STATE_CONSUMING;

                eventStatus = EventStatus::NotConsumed;
            }
        }
		break;
	default:
		break;
	}

	return eventStatus;
}
