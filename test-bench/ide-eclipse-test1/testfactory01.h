#ifndef TESTFACTORY01_H
#define TESTFACTORY01_H


#include "statemachine01.h"

/**
 * \ingroup test01
 *
 * Factory creating all objects used in test1.
 *
 */
class TestFactory01
{
public:
	TestFactory01();		///< Constructor

protected:
	StateMachine01 _task01;		///< First instance of StateMachine01
	StateMachine01 _task02;		///< Second instance of StateMachine01

};

#endif // TESTFACTORY01_H
