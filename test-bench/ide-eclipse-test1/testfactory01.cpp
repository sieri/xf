#include <trace.h>
#include "testfactory01.h"

TestFactory01::TestFactory01()
 : _task01(1000, "Say Hello"),
   _task02(500, "Echo")

{
	Trace::out("Starting test1...\n---------------------");

	// Start state machine
	_task01.startBehavior();
	_task02.startBehavior();
}
