#-------------------------------------------------
#
# Project created by QtCreator 2010-11-19T08:01:08
#
#-------------------------------------------------

QT       += core
QT       -= gui

TARGET = test1
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++11

DEFINES += TC_QTCREATOR

DEPENDPATH += . \

INCLUDEPATH += . \
	../../src/xf \
	../../src/trace

# Add configuration parameters to add real dependency to library(-ies)
CONFIG(debug) {
	LIBS += -L../../src/xf/debug
	PRE_TARGETDEPS += ../../src/xf/debug/libxf.a
}
else {
	LIBS += -L../../src/xf/release
	PRE_TARGETDEPS += ../../src/xf/release/libxf.a
}

CONFIG(debug, debug|release):LIBS += ../../src/xf/debug/libxf.a
CONFIG(release, debug|release):LIBS += .../../src/xf/release/libxf.a

SOURCES += main.cpp \
	../../src/trace/trace.cpp \
	testfactory01.cpp \
    statemachine01.cpp

HEADERS += \
	../../src/trace/trace.h \
	testfactory01.h \
    statemachine01.h
