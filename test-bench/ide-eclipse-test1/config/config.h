#ifndef CONFIG_H_
#define CONFIG_H_

#ifdef BOARD_ARMEBS4
	#include <stdint.h>
	#include <stddef.h>
	#include <stm32/stm32f4xx.h>
	#include <stm32/libstm32_conf.h>		// For asser_param

	#define ASSERT(expr) assert_param(expr)

	#define UNUSED(x) (void)x;

#endif // BOARD_ARMEBS4


#endif /* CONFIG_H_ */
