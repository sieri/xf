#include <trace.h>
#include "testfactory02.h"

TestFactory02::TestFactory02()
 : _pTask01(NULL)
{
	Trace::out("Starting test2...\n---------------------");

	_pTask01 = new StateMachine02();

	if (_pTask01)
	{
		// Start state machine
		_pTask01->startBehavior();
	}
}
