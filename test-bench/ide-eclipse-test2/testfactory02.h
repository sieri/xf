#ifndef TESTFACTORY02_H
#define TESTFACTORY02_H


#include "statemachine02.h"


/**
 * \ingroup test02
 *
 * Factory creating all objects used in test2.
 *
 */
class TestFactory02
{
public:
	TestFactory02();				///< Constructor

protected:
	StateMachine02 * _pTask01;		///< Instance of StateMachine02

};

#endif // TESTFACTORY02_H
